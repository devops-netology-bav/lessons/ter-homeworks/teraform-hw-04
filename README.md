# Домашнее задание к занятию «Продвинутые методы работы с Terraform»

------

### Задание 1

1. Возьмите из [демонстрации к лекции готовый код](https://github.com/netology-code/ter-homeworks/tree/main/04/demonstration1) для создания ВМ с помощью remote-модуля.
2. Создайте одну ВМ, используя этот модуль. В файле cloud-init.yml необходимо использовать переменную для ssh-ключа вместо хардкода. Передайте ssh-ключ в функцию template_file в блоке vars ={} .
Воспользуйтесь [**примером**](https://grantorchard.com/dynamic-cloudinit-content-with-terraform-file-templates/). Обратите внимание, что ssh-authorized-keys принимает в себя список, а не строку.
3. Добавьте в файл cloud-init.yml установку nginx.
4. Предоставьте скриншот подключения к консоли и вывод команды ```sudo nginx -t```.

### Решение

1. Взял готовый код;
2. Добавил в файл cloud-init.yaml:  
```
template=file('./cloud-init.yaml')
```
3.  
![Рис.1-1](img/%D0%A0%D0%B8%D1%81.1-1.png)  
![Рис.1-2](img/%D0%A0%D0%B8%D1%81.1-2.png)  

4. Вывод команды **sudo nginx -t**  
![Рис.1-3](img/%D0%A0%D0%B8%D1%81.1-3.png)

------

### Задание 2

1. Напишите локальный модуль vpc, который будет создавать 2 ресурса: **одну** сеть и **одну** подсеть в зоне, объявленной при вызове модуля, например: ```ru-central1-a```.
2. Вы должны передать в модуль переменные с названием сети, zone и v4_cidr_blocks.
3. Модуль должен возвращать в root module с помощью output информацию о yandex_vpc_subnet. Пришлите скриншот информации из terraform console о своем модуле. Пример: > module.vpc_dev  
4. Замените ресурсы yandex_vpc_network и yandex_vpc_subnet созданным модулем. Не забудьте передать необходимые параметры сети из модуля vpc в модуль с виртуальной машиной.
5. Откройте terraform console и предоставьте скриншот содержимого модуля. Пример: > module.vpc_dev.
6. Сгенерируйте документацию к модулю с помощью terraform-docs.    
 
Пример вызова

```
module "vpc_dev" {
  source       = "./vpc"
  env_name     = "develop"
  zone = "ru-central1-a"
  cidr = "10.0.1.0/24"
}
```
### Решение

1. Взял из примера;
2. Перенес блоки создания сети и подсети в modules->vps_dev;
3-4. в main.tf внес блок подключения модуля vps_dev;
![Рис.2-1](img/%D0%A0%D0%B8%D1%81.2-1.png)  
![Рис.2-2](img/%D0%A0%D0%B8%D1%81.2-2.png)

Снова выполняем команду **terraform init**;  
![Рис.2-3](img/%D0%A0%D0%B8%D1%81.2-3.png)

5. Выполнение команды **terraform console**;  
![Рис.2-4](img/%D0%A0%D0%B8%D1%81.2-4.png)  

6. [**terraform-docs**](https://terraform-docs.io/user-guide/introduction/).  
Предварительно установил **snap install terraform-docs**  
Для генерации документации используеем команду:   
**terraform-docs markdown table --output-file Readme.md ./modules/vpc_dev**

![Рис.2-5](img/%D0%A0%D0%B8%D1%81.2-5.png)

### Задание 3
1. Выведите список ресурсов в стейте.
2. Полностью удалите из стейта модуль vpc.
3. Полностью удалите из стейта модуль vm.
4. Импортируйте всё обратно. Проверьте terraform plan. Изменений быть не должно.
Приложите список выполненных команд и скриншоты процессы.

### Решение:

1. Команда **terraform state list**;
2. Команда **terraform state rm module.vpc_dev**;  
3. Команда **terraform state rm module.test-vm**;  
![Рис.3-1](img/%D0%A0%D0%B8%D1%81.3-1.png)  

4. Последовательно выполняем команды:  

- terraform import module.vpc_dev.yandex_vpc_network.network **id**    
![Рис.3-2](img/%D0%A0%D0%B8%D1%81.3-2.png)  

- terraform import module.vpc_dev.yandex_vpc_subnet.subnet **id**    
![Рис.3-3](img/%D0%A0%D0%B8%D1%81.3-3.png)  

- terraform import module.test-vm.yandex_compute_instance.vm[0] **id**  
- terraform import module.test-vm.yandex_compute_instance.vm[1] **id**    
![Рис.3-4](img/%D0%A0%D0%B8%D1%81.3-4.png)  

- terraform plan  
![Рис.3-5](img/%D0%A0%D0%B8%D1%81.3-5.png)  


## Дополнительные задания (со звёздочкой*)

**Настоятельно рекомендуем выполнять все задания со звёздочкой.**   Они помогут глубже разобраться в материале.   
Задания со звёздочкой дополнительные, не обязательные к выполнению и никак не повлияют на получение вами зачёта по этому домашнему заданию. 


### Задание 4*

1. Измените модуль vpc так, чтобы он мог создать подсети во всех зонах доступности, переданных в переменной типа list(object) при вызове модуля.  
  
Пример вызова
```
module "vpc_prod" {
  source       = "./vpc"
  env_name     = "production"
  subnets = [
    { zone = "ru-central1-a", cidr = "10.0.1.0/24" },
    { zone = "ru-central1-b", cidr = "10.0.2.0/24" },
    { zone = "ru-central1-c", cidr = "10.0.3.0/24" },
  ]
}

module "vpc_dev" {
  source       = "./vpc"
  env_name     = "develop"
  subnets = [
    { zone = "ru-central1-a", cidr = "10.0.1.0/24" },
  ]
}
```

Предоставьте код, план выполнения, результат из консоли YC.

### Решение
- Добавил модуль vpc_prod и подключил его в main.tf;  
- Применил команды **terraform init**, **terraform plan** и **terraform apply**;  
- Результат выполнения:  
![Рис.4-1](img/%D0%A0%D0%B8%D1%81.4-1.png)    
![Рис.4-2](img/%D0%A0%D0%B8%D1%81.4-2.png)    



### Задание 5*

1. Напишите модуль для создания кластера managed БД Mysql в Yandex Cloud с одним или тремя хостами в зависимости от переменной HA=true или HA=false. Используйте ресурс yandex_mdb_mysql_cluster: передайте имя кластера и id сети.
2. Напишите модуль для создания базы данных и пользователя в уже существующем кластере managed БД Mysql. Используйте ресурсы yandex_mdb_mysql_database и yandex_mdb_mysql_user: передайте имя базы данных, имя пользователя и id кластера при вызове модуля.
3. Используя оба модуля, создайте кластер example из одного хоста, а затем добавьте в него БД test и пользователя app. Затем измените переменную и превратите сингл хост в кластер из 2-х серверов.
4. Предоставьте план выполнения и по возможности результат. Сразу же удаляйте созданные ресурсы, так как кластер может стоить очень дорого. Используйте минимальную конфигурацию.

### Задание 6*

1. Разверните у себя локально vault, используя docker-compose.yml в проекте.
2. Для входа в web-интерфейс и авторизации terraform в vault используйте токен "education".
3. Создайте новый секрет по пути http://127.0.0.1:8200/ui/vault/secrets/secret/create
Path: example  
secret data key: test 
secret data value: congrats!  
4. Считайте этот секрет с помощью terraform и выведите его в output по примеру:
```
provider "vault" {
 address = "http://<IP_ADDRESS>:<PORT_NUMBER>"
 skip_tls_verify = true
 token = "education"
}
data "vault_generic_secret" "vault_example"{
 path = "secret/example"
}

output "vault_example" {
 value = "${nonsensitive(data.vault_generic_secret.vault_example.data)}"
} 

Можно обратиться не к словарю, а конкретному ключу:
terraform console: >nonsensitive(data.vault_generic_secret.vault_example.data.<имя ключа в секрете>)
```
5. Попробуйте самостоятельно разобраться в документации и записать новый секрет в vault с помощью terraform. 

### Решение

1. Размернул локально vault, используля docker-compose,  
только указал тег, без тега почему-то не находилобраз  
2-3. Зашел в web и создал секреты;  
![Рис.6-1](img/%D0%A0%D0%B8%D1%81.6-1.png)  

4. считал секреты terraform:  

![Рис.6-2](img/%D0%A0%D0%B8%D1%81.6-2.png)  
![Рис.6-3](img/%D0%A0%D0%B8%D1%81.6-3.png)  


